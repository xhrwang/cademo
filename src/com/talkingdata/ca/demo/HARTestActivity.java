
package com.talkingdata.ca.demo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.talkingdata.sdk.TCAgent;
import com.talkingdata.sdk.TCAgent.DetectedResult;
import com.talkingdata.sdk.TCAgent.POIInfo;

public class HARTestActivity extends Activity {
    public static final String TAG = HARTestActivity.class.getName();
    private StringBuilder strBuilder = new StringBuilder();
    private Button bt_stop_detecting;
    private Button bt_start_detecting;
    private Button bt_curr;
    private Button bt_locs;
    private Button bt_registePOI;
    private Button bt_unregistePOI;

//    private final int WALK = 1;
//    private final int RUN = 2;
//    private final int CYCLING = 3;
//    private final int DRIVING = 4;
//    private final int INDOOR = 5;
//    private final int OUTDOOR = 6;
//    private final int STILL = 7;
    private final HARCallback activityCallback = new HARCallback();
    private final POICallbacks poiCallback = new POICallbacks();
    private Context context = null;
    private LocationManager manager;
    
    private double latitude = 0;
    private double longitude  = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        bt_stop_detecting = (Button) findViewById(R.id.bt_stop);
        bt_start_detecting = (Button) findViewById(R.id.bt_start);
        bt_curr = (Button) findViewById(R.id.bt_curr);
        bt_locs = (Button) findViewById(R.id.bt_locs);
        bt_registePOI = (Button) findViewById(R.id.bt_registePOI);
        bt_unregistePOI = (Button) findViewById(R.id.bt_unregistePOI);
        enableBtns(false, false);
        TCAgent.init(this, "807E05454CEDAC6BD0F2525517DEA2C0", "Beta");
        TCAgent.setReportUncaughtExceptions(true);
        TCAgent.initContextAwarenessModule(this);
        context = getApplicationContext();
        manager = (LocationManager) getSystemService(LOCATION_SERVICE);//获取手机位置信息
        //获取的条件
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);//获取精准位置
        criteria.setCostAllowed(true);//允许产生开销
        criteria.setPowerRequirement(Criteria.POWER_HIGH);//消耗大的话，获取的频率高
        criteria.setSpeedRequired(true);//手机位置移动
        criteria.setAltitudeRequired(true);//海拔
        
        //获取最佳provider: 手机或者模拟器上均为gps
        String bestProvider = manager.getBestProvider(criteria, true);//使用GPS卫星
        
        //parameter: 1. provider 2. 每隔多少时间获取一次  3.每隔多少米  4.监听器触发回调函数
        manager.requestLocationUpdates(bestProvider,100,10, new MyLocationListener());
    }
    
    private class MyLocationListener implements LocationListener{
        /**
         * 手机位置发生变动
         */
        public void onLocationChanged(Location location) {
            latitude = location.getLatitude();//经度
            longitude = location.getLongitude();//纬度
            
            
        }
        
        /**
         * 当某个位置提供者的状态发生改变时
         */
        public void onStatusChanged(String provider, int status, Bundle extras) {
            
        }
        
        
        /**
         * 某个设备打开时
         */
        public void onProviderEnabled(String provider) {
            
        }

        
        /**
         * 某个设备关闭时
         */
        public void onProviderDisabled(String provider) {
            
        }
        
    }

    @Override
    public void onResume() {
        super.onResume();
        setListener();
        TCAgent.registeCurrentActivity(context, activityCallback);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    class getDetectedActivityCallback {
        void onActivityDetected(int activity) {
            // Log.i("rHAR", "Detected activity is " + String.valueOf(activity));
        }
    }

    private void setListener() {

        bt_stop_detecting.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                enableBtns(false, false);
                TCAgent.unregisteCurrentActivity(context, activityCallback);
                
            }
        });

        bt_start_detecting.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                disableBtns(true, false);
                TCAgent.registeCurrentActivity(context, activityCallback);
                TextView tv = (TextView) findViewById(R.id.tv_detectedActivity);
                tv.setText("");
            }
        });

        bt_curr.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                disableBtns(false, false);
                TCAgent.getCurrentPOI(context, poiCallback, (float)latitude, (float)longitude);//;
                TextView tv = (TextView) findViewById(R.id.tv_detectedActivity);
                tv.setText("");
            }
        });

        bt_locs.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                disableBtns(false, false);
                TCAgent.getFrequentlyVisitedPOIList(context, poiCallback);
                TextView tv = (TextView) findViewById(R.id.tv_detectedActivity);
                tv.setText("");
            }
        });

        bt_registePOI.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                disableBtns(false, true);
                TCAgent.registePOINotification(context, poiCallback, null);
                TextView tv = (TextView) findViewById(R.id.tv_detectedActivity);
                tv.setText("");
            }
        });

        bt_unregistePOI.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                enableBtns(false, false);
                bt_unregistePOI.setEnabled(false);
                TCAgent.unregistePOINotification(context, poiCallback);
                TextView tv = (TextView) findViewById(R.id.tv_detectedActivity);
                tv.setText("");
            }
        });
    }
    
    private void disableBtns(boolean isKeepStop, boolean isKeepUnregiste){
        bt_start_detecting.setEnabled(false);
        bt_curr.setEnabled(false);
        bt_locs.setEnabled(false);
        bt_registePOI.setEnabled(false);
        if(isKeepStop){
            bt_stop_detecting.setEnabled(true);
        }
        else {
            bt_stop_detecting.setEnabled(false);
        }
        if(isKeepUnregiste){
            bt_unregistePOI.setEnabled(true);
        }
        else {
            bt_unregistePOI.setEnabled(false);
        }
    }
    
    private void enableBtns(boolean isKeepStop, boolean isKeepUnregiste){
        bt_start_detecting.setEnabled(true);
        bt_curr.setEnabled(true);
        bt_locs.setEnabled(true);
        bt_registePOI.setEnabled(true);
        if(isKeepStop){
            bt_stop_detecting.setEnabled(true);
        }
        else {
            bt_stop_detecting.setEnabled(false);
        }
        if(isKeepUnregiste){
            bt_unregistePOI.setEnabled(true);
        }
        else {
            bt_unregistePOI.setEnabled(false);
        }
    }

    final static class ActionType {

        /**
         * Start algorithm training
         */
        final static int START_TRAINING = 1;

        /**
         * Stop algorithm training
         */
        final static int STOP = 2;

        /**
         * Start detecting current user activity
         */
        final static int START_DETECTING = 3;

        /**
         * Change the label of last feature according to user choice
         */
        final static int CHANGE_LAST_LABEL = 4;
    }
    
    class POICallbacks implements TCAgent.POICallback {

        @Override
        public void onCurrentPOIDetected(POIInfo poiInfo) {
            Handler mainHandler = new Handler(getMainLooper());
            if(poiInfo == null){
                mainHandler.post(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "No current POI data found!", Toast.LENGTH_LONG).show();
                        enableBtns(false, false);
                    }
                });
            }
            else{
                final POIInfo currPOI = poiInfo;
                mainHandler.post(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Detected POI type: " + currPOI.type, Toast.LENGTH_LONG).show();
                        enableBtns(false, false);
                    }
                });
            }
        }

        @SuppressWarnings("unchecked")
        @Override
        public void onFrequentlyVisitedPOIListReady(final Object poiInfos) {
            if(poiInfos instanceof ArrayList<?>){
                ArrayList<TCAgent.POIInfo> poiArr = (ArrayList<TCAgent.POIInfo>)poiInfos;
                Handler mainHandler = new Handler(getMainLooper());
                final int poiArrSize = poiArr.size();
                StringBuilder sb = new StringBuilder();
                sb.append(String.format("Totally %d POIs found: ", poiArrSize));
                for(POIInfo pInfo : poiArr){
                    sb.append("\t");
                    sb.append(pInfo.name);
                    sb.append("\n");
                }
                final String output = sb.toString(); 
                mainHandler.post(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), output, Toast.LENGTH_LONG).show();
                        enableBtns(false, false);
                    }
                });
            }
        }

        @Override
        public float currentLatitude() {
            return 37;
        }

        @Override
        public float currentLongitude() {
            return 73;
        }

        @Override
        public void onPOIEvent(POIInfo poiInfo, int poiStatus) {
            Handler mainHandler = new Handler(getMainLooper());
            StringBuilder sb = new StringBuilder();
            sb.append("Detected POI ");
            sb.append(poiInfo.name);
            sb.append(", status: ");
            if(poiStatus == 1){
                sb.append("Enter");
            }else{
                sb.append("Leave");
            }
            final String output = sb.toString();
            mainHandler.post(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), output, Toast.LENGTH_LONG).show();
                    enableBtns(false, false);
                }
            });
        }

        @Override
        public void onUnregisteSucceed() {
            Handler mainHandler = new Handler(getMainLooper());
            mainHandler.post(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), "Successfully unregisted POI.", Toast.LENGTH_LONG).show();
                    enableBtns(false, false);
                }
            });
        }

        @Override
        public void onError() {
            Handler mainHandler = new Handler(getMainLooper());
            mainHandler.post(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), "Error occured...", Toast.LENGTH_LONG).show();
                    enableBtns(false, false);
                }
            });
        }
        
    }

    class HARCallback implements TCAgent.CurrentActivityCallback {
        public void onAutoStop(final String msg) {
            Handler mainHandler = new Handler(getMainLooper());
            mainHandler.post(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                    enableBtns(false, false);
                    strBuilder = new StringBuilder();
                }
            });
        }

        public void update(final String msg) {
            Handler mainHandler = new Handler(getMainLooper());
            mainHandler.post(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                }
            });
        }

        public void onError() {
            Handler mainHandler = new Handler(getMainLooper());
            mainHandler.post(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), "Error occured during activity recognition", Toast.LENGTH_LONG).show();
                }
            });
        }

        @SuppressLint("SimpleDateFormat")
        public void onResult(final DetectedResult[] detectedResults) {
            strBuilder = new StringBuilder();
            Date date = new Date();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
            strBuilder.append(df.format(date));
            strBuilder.append(": ");
            StringBuilder detectStrBdr = new StringBuilder();
            String mostProbActivity = "";
            float maxProb = 0F;
            for (int i = 0; i < detectedResults.length; ++i) {
                StringBuilder singleActivityStrBdr = new StringBuilder();
                int act = detectedResults[i].getActivityType();
//                int label1 = act / 1000;
//                if (label1 == INDOOR) {
//                    singleActivityStrBdr.append("室内_");
//                } else if (label1 == OUTDOOR) {
//                    singleActivityStrBdr.append("室外_");
//                }
                int label2 = act % 1000;
                if (label2 == TCAgent.DetectedActivity.WALK) {
                    singleActivityStrBdr.append("步行");
                } else if (label2 == TCAgent.DetectedActivity.RUN) {
                    singleActivityStrBdr.append("跑步");
                } else if (label2 == TCAgent.DetectedActivity.CYCLING) {
                    singleActivityStrBdr.append("骑车");
                } else if (label2 == TCAgent.DetectedActivity.DRIVING) {
                    singleActivityStrBdr.append("乘车");
                } else if (label2 == TCAgent.DetectedActivity.STILL) {
                    singleActivityStrBdr.append("静止");
                }
                float prob = Float.isNaN(detectedResults[i].getActivityPossibility()) ? 0 : detectedResults[i].getActivityPossibility();
                String currentActivity = singleActivityStrBdr.toString();
                detectStrBdr.append(String.format("\t%-15s \t %10.2f%%\n",
                        singleActivityStrBdr.toString(),
                        prob * 100));
                if (Float.compare(maxProb, prob) < 0) {
                    maxProb = prob;
                    mostProbActivity = currentActivity;
                }
            }
            if(mostProbActivity.trim().isEmpty()){
                strBuilder.append("Unknown\n");
            }else{
                strBuilder.append(mostProbActivity);
                strBuilder.append("\n");
                strBuilder.append(detectStrBdr);
            }
            String output = strBuilder.toString();
            Log.i("rHAR", output);
            Handler mainHandler = new Handler(getMainLooper());
            mainHandler.post(new Runnable() {

                @Override
                public void run() {
                    TextView tv = (TextView) findViewById(R.id.tv_detectedActivity);
                    strBuilder.append(tv.getText());
                    tv.setText(strBuilder.toString());
                }
            });
        }
    }
}
