# Context Awareness SDK 集成文档

---

本文档是 TalkingData Context Awareness SDK 的集成文档。提供下面的功能：
 - 实时用户行为识别
 - 基于当前位置的 POI（Point of interest）信息获取
 - 访问频率最高的 POI 位置信息获取
 - POI 进入/离开事件订阅

## 集成方法

由于 Context Awareness SDK 使用了 `Locale Service`，所以需要开发者在 `AndroidManifest.xml` 文件中添加下面的内容：

    <service android:name="com.talkingdata.sdk.ModuleCAService" >

同时，由于 POI 相关信息需要云端支持，需要开发者添加下面的权限声明：

    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />

### 初始化

Context Awareness SDK 是在 [TalkingData Analytics SDK](https://www.talkingdata.com/app/document_web/index.jsp?statistics) 基础上添加了 Context Awareness 相关功能的增强版 SDK。需要在 Analytics SDK 初始化后对 Context Awareness 模块进行初始化。接口方法如下：

    /**
     * 获取当前用户行为
     * 
     * @param ctx
     * @return
     */
    public static final void initContextAwarenessModule(final Context ctx)

### 实时用户行为识别

初始化之后，调用下面的方法获取实时用户行为：

    /**
     * 订阅当前用户行为实时识别的结果
     * 
     * @param ctx
     * @param callback 实时用户行为识别完成后调用的回调函数
     * @return
     */
    public static final void registeCurrentActivity(final Context ctx,
            final TCAgent.CurrentActivityCallback callback)

实现 TCAgent.CurrentActivityCallback 接口后，每次检测出实时用户行为后，将调用该接口中定义的 `onResult(final DetectedResult[] detectedResults)` 方法，接口详情为：

    /**
     * 获取实时用户行为的回调接口
     * 
     */
    public interface CurrentActivityCallback {

        /**
         * 发生错误时的回调方法
         */
        void onError();
        
        /**
         * 实时用户行为识别自动停止后的回调方法
         */
        void onAutoStop(final String msg);
        
        /**
         * 实时用户行为识别的状态更新回调方法
         */
        void update(final String msg);
        
        /**
         * 实时用户行为识别结果更新的回调方法
         */
        void onResult(final DetectedResult[] detectedResults);
    }


`DetectedResult` 的定义为：

	/**
     * 实时用户行为识别结果
     */
    public static class DetectedResult {
        
        /**
         * 实时用户行为类型
         */
        private int activityType;
        
        /**
         * 实时用户行为类型的可能性
         */
        private float possibility;
        
        public DetectedResult(){
            this(DetectedActivity.UNKNOWN, 0);
        }
        public DetectedResult(final int activityType, final float activityPossibility){
            this.activityType = activityType;
            this.possibility = activityPossibility;
        }
        
        public int getActivityType(){
            return this.activityType;
        }
        
        public float getActivityPossibility(){
            return this.possibility;
        }
    }

其中 `activityType` 的定义在 `TCAgent` 中的 `DetectedActivity` 类中：

	/**
     * 实时用户行为类型
     */
    public class DetectedActivity{
        
        /**
         * 走
         */
        public final static int WALK = 1;
        
        /**
         * 跑
         */
        public final static int RUN = 2;
        
        /**
         * 骑车
         */
        public final static int CYCLING = 3;
        
        /**
         * 乘车
         */
        public final static int DRIVING = 4;
        
        /**
         * 静止
         */
        public final static int STILL = 7;
        
        /**
         * 位置
         */
        public final static int UNKNOWN = 0;
        
    }

使用下面的接口方法停止实施用户行为识别：

    /**
     * 取消订阅当前用户行为实时识别的结果
     * 
     * @param ctx
     * @param callback 实时用户行为识别完成后调用的回调函数
     * @return
     */
    public static final void unregisteCurrentActivity(final Context ctx,
            final TCAgent.CurrentActivityCallback callback)

### 基于当前位置的 POI（Point of interest）信息获取

调用下面的接口方法获取用户当前位置的 POI 信息：

    /**
     * 获取当前地点可能的 POI 信息
     * 
     * @param ctx
     * @param callback 实时用户行为识别完成后调用的回调函数
     * @param latitude 维度
     * @param longitude 经度
     * @return
     */
    public static final void getCurrentPOI(final Context ctx,
            final TCAgent.POICallback callback,
            final float latitude, final float longitude)

### 访问频率最高的 POI 位置信息获取

调用下面的接口方法获取用户访问频率最高的 POI 位置信息列表：

    /**
     * 获取当前地点可能的 POI 信息
     * 
     * @param ctx
     * @param callback 实时用户行为识别完成后调用的回调函数
     * @return
     */
    public static final void getFrequentlyVisistedPOIList(final Context ctx, 
            final TCAgent.POICallback callback)

### POI 进入/离开事件订阅

调用下面的方法订阅用户访问频率最高的 POI 信息的 `进入/离开` 事件：

    /**
     * 订阅 POI进入/离开事件
     * 
     * @param ctx
     * @param callback 实时用户行为识别完成后调用的回调函数
     * @return
     */
    public static final void registePOINotification(final Context ctx,
            final TCAgent.POICallback callback,
            final POIInfo poiInfo)

调用下面的方法取消订阅用户访问频率最高的 POI 信息的 `进入/离开` 事件：

    /**
     * 取消订阅 POI进入/离开事件
     * 
     * @param ctx
     * @param callback 实时用户行为识别完成后调用的回调函数
     * @return
     */
    public static final void unregistePOINotification(final Context ctx,
            final TCAgent.POICallback callback)

接口 `TCAgent.POICallback` 的细节为：

    /**
     * POI信息相关回调接口
     */
    public interface POICallback {
        
        /**
         * 更新当前检测到的 POI 信息
         */
        void onCurrentPOIDetected(final POIInfo poiInfo);
        
        /**
         * 更新访问频率最高的 POI 列表
         */
        void onFrequentlyVisitedPOIListReady(final Object poiInfos);
        
        /**
         * SDK 获取当前经度的回调方法
         */
        float currentLatitude();
        
        /**
         * SDK 获取当前纬度的回调方法
         */
        float currentLongitude();
        
        /**
         * 指定 POI 进入/离开事件发生时的回调方法
         */
        void onPOIEvent(final POIInfo poiInfo, final int poiStatus);
        
        /**
         * 成功取消注册 POI 进入/离开事件的回调方法
         */
        void onUnregisteSucceed();
        
        /**
         * SDK 发生内部错误时的回调方法
         */
        void onError();
    }

其中，`POIInfo` 的定义为：

	/**
     * 实时用户行为识别自动停止后的回调方法
     */
    public static class POIInfo {
        
        /**
         * POI 坐标列表，当前版本返回空
         */
        public ArrayList<CoreUtils.BmapPoint> coords = new ArrayList<CoreUtils.BmapPoint>();
        
        /**
         * POI 地址
         */
        public String address = "";
        
        /**
         * POI 所在城市
         */
        public String city = "";
        
        /**
         * POI 名称
         */
        public String name = "";
        
        /**
         * POI Id
         */
        public String uid = "";
        
        /**
         * POI 类型，比如 Office，Home
         */
        public String type = "";
        
    }

`POIStatus` 的定义为：

	/**
     * POI 进入/离开状态
     */
    public static class POIStatus{
        
        /**
         * 进入 POI 状态
         */
        public final static int ENTER = 1;
        
        /**
         * 离开 POI 状态
         */
        public final static int LEAVE = 2;
    }

`进入/离开` 事件发生时，`void onPOIEvent(final POIInfo poiInfo, final int poiStatus)` 回调函数会被调用。